package client;

import java.util.ArrayList;
import java.util.List;

import datastructures.Page;
import datastructures.Queue;
import datastructures.ST;
import io.Out;
import util.WikiUtil;

/** WikiIndexClient calls WikiUtil methods to index pages from a wiki.*/
public class WikiIndexClient 
{
	WikiUtil wiki;
	
	public WikiIndexClient(String username, String password, String wikiSite)
	{
		wiki = new WikiUtil(username, password, wikiSite);
	}
	
	/** (#1a) indexPages should start with a seed page (e.g., if you wanted to start with the “Education” page, you can call WikiUtil.getPageText(“Education”)), 
	 * and the maximum number of pages you want to index. It should first store the seed page as a page, and then follow all of the links on that page to other pages, 
	 * and store each other linked-to page as a page. That is, the closer pages to the seed page should be indexed first. Your indexing method should stop once you have 
	 * indexed the maximum number of pages.
	 * @param pageSeed
	 * @param maxPages
	 * @return
	 */
	public ST<String, Page> indexPages(String pageSeed, int maxPages)
	{
		ST<String, Page> pages = new ST<String, Page>();
        Queue<Page> q = new Queue<Page>();
        q.enqueue(new Page(pageSeed, wiki.getPageText(pageSeed), wiki.getPageLinks(pageSeed)));
		while (!q.isEmpty()) 
		{
			Page page = q.dequeue();
			pages.put(page.getTitle(), page);
			
		    for (String linkedPage : page.getLinks()) 
		    {
		    	if (!pages.contains(linkedPage) && pages.size() + q.size() < maxPages) 
		    	{
		            q.enqueue(new Page(linkedPage, wiki.getPageText(linkedPage), wiki.getPageLinks(linkedPage)));
		    	}
		    }
			
		}
	    return pages;
	}
	
	/** (#1b) Writes a collection of pages to one or more files in order to save the information.*/
	public void writePagesToFile(ST<String,Page> pages, String filename)
	{
		Out outfile = new Out("src/data/" + filename+"-graph.txt");
		Out outfile2 = new Out("src/data/" + filename+"-index.txt");

		for (String page : pages)
		{
			ArrayList<String> links = new ArrayList<String>(pages.get(page).getLinks());
			ArrayList<String> keywords = new ArrayList<String>(pages.get(page).getKeywords());
			for (String link : links)
				outfile.println(page + "|" + link);
			outfile2.print(page);
			for (String keyword : keywords)
				 outfile2.print("|"+keyword);
			outfile2.print("\n");
		}
	}
	
	public static void main(String[] args)
	{
		WikiIndexClient client = new WikiIndexClient("your name here", "your password here", "en.wikiquote.org");
		ST<String, Page> hashPages = client.indexPages("Education", 1000);
		for (String page : hashPages)
		{
			System.out.println(page);
		}
		client.writePagesToFile(hashPages, "wikiOutput");
	}
	
}