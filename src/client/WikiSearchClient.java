package client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.TreeMap;

import javax.security.auth.login.LoginException;

import datastructures.Page;
import datastructures.WikiPages;
import io.StdIn;
import io.StdOut;
import jwiki.core.Wiki;
import util.WikiUtil;

/** Client for searching a wiki for keywords.*/
public class WikiSearchClient
{
	WikiPages wp;
	
	public WikiSearchClient(String graphName, String indexName)
	{
	    wp = new WikiPages();
		wp.createGraph(graphName);
		wp.createIndex(indexName);
	}
	
	public ArrayList<String> searchKeywords(ArrayList<String> keywords, int max)
	{
		return wp.searchKeywords(keywords, max);
	}
	
	public ArrayList<String> findRelatedPages(ArrayList<String> pages, int max)
	{
		return wp.findRelevantPages(pages, max);
	}
	
	/** (#6) Asks the user to input a list of keywords, and then call searchKeyword and findRelevantPages to 
	 * return and output two ordered lists, one that outputs the results of searchKeyword, and the other that 
	 * outputs the results of findRelevantPages.
	 * @param args
	 */
	public static void main(String[] args) 
	{
		WikiSearchClient searchClient = new WikiSearchClient("src/data/wikiOutput-graph.txt", "src/data/wikiOutput-index.txt");
		
		StdOut.println("Enter a list of keywords separated by spaces. Hit enter when done. Type quit to quit.");
		while (StdIn.hasNextLine()) 
     	{
			String source = StdIn.readLine();
     		if (source.equals("quit"))
     			break;
     		else
     		{
     			String[] sourceArray = source.split(" ");
     			if (sourceArray != null)
     			{
     				ArrayList<String> keywords = new ArrayList<String>(Arrays.asList(sourceArray));
     				ArrayList<String> foundPages = searchClient.searchKeywords(keywords, 100);
     				ArrayList<String> relatedPages = searchClient.findRelatedPages(foundPages, 10);
     				System.out.println("Pages found: " + foundPages);
     				System.out.println("Related pages: " + relatedPages);
     			}

    		}
     		
    		StdOut.println("Enter a list of keywords separated by spaces. Hit enter when done. Type quit to quit.");
     	}
		
		 
	}
	
}
