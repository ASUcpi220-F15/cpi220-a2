package datastructures;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

import datastructures.Page;
import edu.uci.ics.jung.graph.Graph;
import io.StdOut;

public class InvertedIndex {
	
	private ST<String, Set<String>> index;
	
	public InvertedIndex()
	{
		index = new ST<String, Set<String>>();
	}
	
	/**
	 * This method reads a graph and generates the inverted index. This structure
	 * indexes pages by their keywords. So if you're searching for a page that has a keyword,
	 * this structure will quickly give you the set of pages that have it
	 * @param graph
	 */
	/*public void readGraph(Graph<Page, LinkType> graph){
		Collection<Page> pages = graph.getVertices();
		for (Page page : pages) {
			List<String> keywords = page.getKeywords();
			for (String keyword : keywords) {
				putKeyword(keyword, page);
			}
		}
	}*/
	
	public void putKeyword(String keyword, String page){
		if(!index.contains(keyword)){
			index.put(keyword, new HashSet<String>());
		}
		index.get(keyword).add(page);
	}
	
	public Set<String> findPage(String keyword){
		return index.get(keyword);
	}
	
	public List<String> search(String[] keywords){
		List<String> results = null;
		PriorityQueue<String> queue = new PriorityQueue<String>();
		// TODO
		// Retrieve pages for each keyword
		// Merge the sets and put a high priority on those that appear on more than one
		//	(Ordered by number of appearances) 
		return results;
	}
	
	public void printIndex()
	{
		for (String key : index.keys())
		StdOut.println(key + " - " + index.get(key));
	}
}
