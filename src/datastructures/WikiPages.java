package datastructures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import digraph.DepthFirstDirectedPaths;
import digraph.SymbolDigraph;
import datastructures.InvertedIndex;
import io.In;
import io.StdOut;

/** Should store information about a collection of wiki pages.*/
public class WikiPages 
{
	public final String DELIMITER = "\\|";
	
	SymbolDigraph sd;
	ST pages;
	InvertedIndex keywordIndex;
	IndexMaxPQ<Integer> maxPQ;
	IndexMaxPQ<Integer> maxPQRelevant;

	public WikiPages()
	{
		maxPQ = new IndexMaxPQ<Integer>(10000);
		maxPQRelevant = new IndexMaxPQ<Integer>(10000);
	}
	
	/** (#2) Reads in data from a file and stores it as a graph.*/
	public void createGraph(String filename)
	{
		sd = new SymbolDigraph(filename, DELIMITER);
		
	}
	
	/** (#3) Reads in data from a file and stores it in an index.*/
	public void createIndex(String filename)
	{
		In in = new In(filename);
        pages = new ST<String, ArrayList<String>>();
    	keywordIndex = new InvertedIndex();
    	while (in.hasNextLine())
    	{
    		String str = in.readLine();
    		String strArray[] = str.split("\\|");
    		ArrayList<String> list = new ArrayList<String>();
    		for (int i = 1; i < strArray.length; i++)
    		{
    			list.add(strArray[i]);
    			keywordIndex.putKeyword(strArray[i], strArray[0]);
    		}
    		pages.put(strArray[0], list);		
    	}
		
	}
	
	/** (#4) Takes a list of keywords and the maximum number of pages to return, returns an ordered list of pages
	 * that contain those keywords.*/
	public ArrayList<String> searchKeywords(ArrayList<String> keywords, int max)
	{
		for (String keyword : keywords)
		{
			Set<String> pageTitles = keywordIndex.findPage(keyword);
			if (pageTitles != null)
			{
				for (String page : pageTitles)
				{	
					int pageIndex = sd.index(page);
					if (!maxPQ.contains(pageIndex))
					{
						maxPQ.insert(pageIndex, 1);
					}
					else
					{
						int newIndex = maxPQ.keyOf(pageIndex)+1;
						maxPQ.increaseKey(pageIndex, newIndex);
					}
				}
			}
			
		}
		ArrayList<String> returnList = new ArrayList<String>();
		for (int i = 0; i < max && !maxPQ.isEmpty(); i++)
			returnList.add(sd.name(maxPQ.delMax()));
		return returnList;
	}
	
	/** (#5) Takes a list of result pages and the maximum number of relevant pages to return, returns an ordered list 
	 * of pages reachable from the results pages.*/
	public ArrayList<String> findRelevantPages(ArrayList<String> results, int max)
	{
		for (String page : results)
		{
			new DepthFirstDirectedPaths(sd.G(), sd.index(page), maxPQRelevant);
		}
       
		ArrayList<String> relevantPages = new ArrayList<String>();
		for (int i = 0; i < max && !maxPQRelevant.isEmpty(); i++)
		{
			relevantPages.add(sd.name(maxPQRelevant.delMax()));
		}
		return relevantPages;
	}
	
	public SymbolDigraph getGraph()
	{
		return sd;
	}
	
	public InvertedIndex getIndex()
	{
		return keywordIndex;
	}
	
	public static void main(String[] args)
	{
		//Test #2
		StdOut.println("\nTESTING QUESTION 2");
		WikiPages wp = new WikiPages();
		wp.createGraph("src/data/routes.txt");
		wp.getGraph().printGraph();
		StdOut.println();
		
		//Test #3
		StdOut.println("\nTESTING QUESTION 3");
		wp.createIndex("src/data/routesKeywords.txt");
		wp.getIndex().printIndex();
		
		//Test #4
		StdOut.println("\nTESTING QUESTION 4");
		ArrayList<String> keywords = new ArrayList<String>(Arrays.asList("Local", "Airport", "Dallas", "Whatever"));
		ArrayList<String> pages = wp.searchKeywords(keywords, 1);
		StdOut.println(pages);
		
		//Test #5
		StdOut.println("\nTESTING QUESTION 5");
		ArrayList<String> relatedPages = wp.findRelevantPages(pages, 6);
		System.out.println(relatedPages);
	}
}
