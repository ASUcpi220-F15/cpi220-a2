package util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/** TextParserUtil is a utility class for parsing text.*/
public class TextParserUtil {

	/** Extract keywords accepts a String as input, strips it of non-letter characters, 
	 * and then returns an ArrayList of the words in the String.
	 * @param pageText
	 * @return
	 */
	public static List<String> extractKeywords(String pageText) 
	{
		ArrayList<String> list = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(pageText);
	     while (st.hasMoreTokens()) {
	    	 String token = st.nextToken();
	    	 token = token.replaceAll("[^a-zA-Z ]", "").toLowerCase();
	         list.add(token);
	     }
		return list;
	}
}
